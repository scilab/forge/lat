#LyX 1.6.7 created this file. For more info see http://www.lyx.org/
\lyxformat 345
\begin_document
\begin_header
\textclass paper
\use_default_options true
\begin_modules
theorems-ams
eqs-within-sections
figs-within-sections
\end_modules
\language english
\inputencoding auto
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\cite_engine natbib_authoryear
\use_bibtopic false
\paperorientation portrait
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\author "" 
\author "" 
\end_header

\begin_body

\begin_layout Title
Lattice Algebra Toolbox (LAT) for Scilab
\end_layout

\begin_layout SubTitle
Version 0.2
\end_layout

\begin_layout Author
Miguel Angel Veganzones
\end_layout

\begin_layout Institution
Grupo de Inteligencia Computacional, Universidad del País Vasco, Spain
\end_layout

\begin_layout Abstract
This document is intended to be the reference manual for the Lattice Algebra
 Toolbox (LAT).
 It includes how to download the sources and install the toolbox in Scilab,
 as well as how to use the functions provided in the toolbox.
 Each function is explained with examples together with some introductory
 theory.
 This manual is not intended to be a extent review on Lattice Algebra neither
 on Lattice Computing.
\end_layout

\begin_layout Section
Introduction
\end_layout

\begin_layout Standard
Lattice Algebra defines on the bounded lattice ordered group 
\begin_inset Formula $\left(\mathbb{R}_{\pm\infty},\vee,\wedge,+,+'\right)$
\end_inset

 as the alternative to the algebraic field 
\begin_inset Formula $\left(\mathbb{R},+,\cdot\right)$
\end_inset

 commonly used for the definition of Neural Network algorithms.
 Here 
\begin_inset Formula $\mathbb{R}$
\end_inset

 denotes the set of real numbers, 
\begin_inset Formula $\mathbb{R}_{\pm\infty}$
\end_inset

 the extended real numbers, 
\begin_inset Formula $\wedge$
\end_inset

 and 
\begin_inset Formula $\vee$
\end_inset

 denote, respectively, the binary 
\begin_inset Formula $\max$
\end_inset

 and 
\begin_inset Formula $\min$
\end_inset

 operations, and 
\begin_inset Formula $+$
\end_inset

, 
\begin_inset Formula $+'$
\end_inset

 denote addition and its dual operation defined by 
\begin_inset Formula $a+'b=a+b$
\end_inset

.
 Lattice Computing can be defined as the collection of computational methods
 that either are defined on the algebra of lattice operators or that employ
 lattice theory to generalize previous approaches.
\end_layout

\begin_layout Standard
Lattice Algebra Toolbox (LAT) provides some basic lattice operations as
 lattice conjugate, max and min product, etc.; as well as some computational
 methods based on the lattice algebra, for instance, the Lattice Associative
 Memories (LAMs).
\end_layout

\begin_layout Section
Copyright
\end_layout

\begin_layout Standard
Lattice Algebra Toolbox is copyright (C) of the Grupo de Inteligencia Computacio
nal, Universidad del País Vasco (UPV/EHU), Spain released under the terms
 of the GNU General Public License.
 Lattice Algebra Toolbox is free software: you can redistribute it and/or
 modify it under the terms of the GNU General Public License as published
 by the Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.
 Lattice Algebra Toolbox is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along
 with Lattice Algebra Toolbox.
 If not, see <http://www.gnu.org/licenses/>.
\end_layout

\begin_layout Section
Usage
\end_layout

\begin_layout Standard
Lattice Algebra Toolbox (LAT) works over Scilab
\begin_inset Foot
status collapsed

\begin_layout Plain Layout
http://www.scilab.org/
\end_layout

\end_inset

, the open source numerical computation software.
 Before using LAT, you'll need to install Scilab in your computer.
 Scilab is free and easy to install, and runs in Linux, Windows and MacOsX
 platforms.
 It is convenient to install the latest stable version available for your
 platform.
 At this moment, latest version is Scilab 5.3.0.
\end_layout

\begin_layout Standard
Once you have installed Scilab, you need to download LAT sources.
 LAT stable versions are available as compressed (.zip and .tar.gz) files from
 Scilab Forge site
\begin_inset Foot
status collapsed

\begin_layout Plain Layout
http://forge.scilab.org/index.php/p/lat/
\end_layout

\end_inset

 (see figure 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:LAT-download"

\end_inset

).
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename images/lat-download.png
	lyxscale 30
	width 100text%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:LAT-download"

\end_inset

Lattice Algebra Toolbox (LAT) package download site
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
It is also possible to obtain the latest unstable developer version by using
 Subversion
\end_layout

\begin_layout Standard
Lattice Algebra Toolbox is being built following the recomendations provided
 by Scilab documentation
\begin_inset Foot
status collapsed

\begin_layout Plain Layout
http://www.scilab.org/support/documentation
\end_layout

\end_inset

 and Scilab community
\begin_inset Foot
status collapsed

\begin_layout Plain Layout
http://wiki.scilab.org/
\end_layout

\end_inset

.
\end_layout

\begin_layout Section
Basic lattice operators
\end_layout

\begin_layout Section
Lattice Computing methods
\end_layout

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
btprint "btPrintAll"
bibfiles "lat"
options "plain"

\end_inset


\end_layout

\end_body
\end_document
