lattice_algebra_toolbox (version 0.2)
    * version 0.1 functions completed, documented and tested
    * Functions implemented: lat_convexpolytope, lat_isdependent, lat_maxmincorners, lat_pointwisemax, lat_pointwisemin

lattice_algebra_toolbox (version 0.1)
    * Functions implemented: lat_conjugate, lat_ismaxfixedpoint, lat_isminfixedpoint, lat_laam, lat_lham, lat_maxproduct, lat_minproduct, lat_minimaxsum

 -- Miguel Angel Veganzones <miguelangel.veganzones@ehu.es>
 -- Grupo de Inteligencia Computacional <http://www.ehu.es/computationalintelligence>
 -- Universidad del País Vasco (UPV/EHU), Spain
 -- Mon, 21 Feb 2011


