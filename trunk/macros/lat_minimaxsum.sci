// ====================================================================
// This file is part of the Lattice Algebra Toolbox for Scilab 5.x
// Copyright (C) Grupo de Inteligencia Computacional, Universidad del País Vasco (UPV/EHU), Spain
// released under the terms of the GNU General Public License
//
// Lattice Algebra Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lattice Algebra Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lattice Algebra Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
// Description: calculates the lattice minimax sum of a set of real vectors and a matrix of real scalars.
//
// see Lattice Algebra Toolbox (help)
// ====================================================================
function s = lat_minimaxsum(valA,valB)
  // Check parameters
  [r_valA, k1] = size(valA);
  [k2, J] = size(valB);
  if (k1 <> k2) then
    error('Incorrect dimensions: see Lattice Algebra Toolbox help');
  else
    k = k1;
  end
  // Maxmin sum
  b = zeros(r_valA,J);
  for j=1:J
    a = zeros(r_valA,k);
    for i=1:k
      a(:,i) = valA(:,i) + valB(i,j); 
    end
    b(:,j) = min(a,'c');
  end
  s = max(b,'c');
endfunction
// ====================================================================
