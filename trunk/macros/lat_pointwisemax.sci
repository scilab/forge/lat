// ====================================================================
// This file is part of the Lattice Algebra Toolbox for Scilab 5.x
// Copyright (C) Grupo de Inteligencia Computacional, Universidad del País Vasco (UPV/EHU), Spain
// released under the terms of the GNU General Public License
//
// Lattice Algebra Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lattice Algebra Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lattice Algebra Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
// Description: calculates the lattice pointwise maximum of two real matrices.
//
// see Lattice Algebra Toolbox (help)
// ====================================================================
function s = lat_pointwisemax(valA,valB)
  // Check parameters
  [r_valA, c_valA] = size(valA);
  [r_valB, c_valB] = size(valB);
  if (r_valA <> r_valB) | (c_valA <> c_valB) then
    error('Incorrect dimensions: see Lattice Algebra Toolbox help');
  end
  // maximum
  s = max(valA,valB);
endfunction
// ====================================================================
